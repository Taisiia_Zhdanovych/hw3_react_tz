
import "./modal.scss";


function Modal(props) {
  
  console.log(props);
  return (
    <div className={props.active ? "modal active" : "modal"} onClick={props.onClick}>
      <div className={props.active ? "modal__container active" : "modal__container"} onClick={(e) => e.stopPropagation()}>
        <div className="modal__header">
          <span className="modal__header-text">{props.headerText}</span>
          <span className={props.active ? "modal__close" : ""} onClick={props.onClick}>X</span>
        </div>
        <p className="modal__main-text">{props.mainText}</p>
        <div className="modal__btn-box">
          <button className="modal__button" onClick={() => props.addToCart(props.id)}>Так</button>
          <button className="modal__button" onClick={props.onClick}>Ні</button>
        </div>
      </div>
    </div>
  );
}

export default Modal;






