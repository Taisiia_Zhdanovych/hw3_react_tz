


import CardItem from "../ProductItem/CardItem";



 



function ProductList (props) {

 
 
  
  const addToFavorite = (id) => {
       
    props.addToFavorite(id);
  }


  const  handleModalActive = (active, id) => { props.handleModalActive(active, id) }
   




  return (
    <>
      
      {props.cardProducts.map((item) => {
        return (
          <CardItem
            favorite={props.favorits?.includes(item.id)}
            key={item.id}
          
            {...item}
             addToFavorite={addToFavorite}
            buttonText={'Add to Cart'}

             setModalActive={handleModalActive}
            {...props.childrenProps}
          />
        );
      })}

                        
         
        
      
                </>
            )
        }
    

export default ProductList;











