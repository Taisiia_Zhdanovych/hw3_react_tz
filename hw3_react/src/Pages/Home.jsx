import React, { useState, useEffect } from "react";
import { getDataFromLS } from "../Helpers/parseHelper"

import Header from "../components/Header/Header";
import Modal from "../components/Modal/Modal";
import { Outlet } from "react-router-dom";

export function Home (props) {
  
   const [cardProducts, setCardProducts] = useState([]);
  const [favorits, setFavorits] = useState( getDataFromLS("favorits") );
  const [cart, setCart] = useState( getDataFromLS ("cart"));
  
  const [modalActive, setModalActive] = useState(false);
  const [modalId, setModalId] = useState(0);
  const [headerText, setHeaderText] = useState("");
  const [mainText, setMainText] = useState("");
 const [modalHandler, setModalHandler] = useState(null)
 

useEffect(() => {
    
      fetch("./ProductData.json")
        .then((res) => res.json())
    .then((data) => setCardProducts(data));
  
 
}, []);
  

  
  

  // Создаем ссылку на предыдущее состояние

  useEffect(() => {
    
      localStorage.setItem('favorits', JSON.stringify(favorits));
    
  
      localStorage.setItem('cart', JSON.stringify(cart));
   
  }, [favorits, cart]);

 

  
  
  
  const addToFavorite = (id) => {
    if (favorits.includes(id)) {
      const newArrD = favorits.filter((item) => item !== id);
      
      setFavorits(newArrD);
      
    } else {
      const newArr = [...favorits, id];
     
      setFavorits(newArr);
     
    }
  };

  

 const handleModalActive = (active, id) => {
  setModalHandler("addToCart")
   if (cart.includes(id)) {
     setHeaderText("Зверніть увагу");
      setMainText("Toвар вже у корзині");
    } else {
     setHeaderText("Додати товар у корзину");
      setMainText("Чи дійсно ви бажаєте додати товар у корзину?");
    }
    setModalId(id);
    setModalActive(active);
}

  

  function addToCart() {
  
    if (!cart.includes(modalId)) {
      const newArr = [...cart, modalId];
      setCart(newArr);
      
    }
    setModalActive(false);
  }



  
  function cartModalActive(active, id) {
   setModalHandler("deleteFromCart")
  
        setHeaderText("Видалення");
        setMainText("Чи дійсно ви бажаєте видалити товар із корзини?");
        setModalId(id);
        setModalActive(active);
    }
     
  function deleteFromCart(id=1) {
    console.log(id);
        
        const newArrD = cart.filter((item) => item !== id);
      
       setCart(newArrD)
        setModalActive(false);
    
    }

  function checkHandler(value) {
    switch (value) {
      case "deleteFromCart":
        
        return deleteFromCart;
    
        case "addToCart":
       return addToCart;
      
      default:
        return () => alert('Try again')   
    }
  }

  return (
        <>
            
        <section className="py-4 container">
          <div className="row juctify-content-center">
            <h1 className="text-center mt-3">
        <Header
            favCounter={favorits.length}
            cartCounter={cart.length}
           primary={favorits.length > 0}
        />
      </h1>
           
          <Outlet context={{cardProducts, favorits, cart, addToFavorite, addToCart, handleModalActive, cartModalActive, deleteFromCart}} />
             <Modal
          active={modalActive}
          id={modalId}
          headerText={headerText}
          mainText={mainText}
          addToCart={checkHandler(modalHandler)}
           onClick={() => setModalActive(false, 0)}
         
        />
          </div>
        </section>
      </>
    );
  

  }





