


import { useOutletContext } from "react-router-dom";

import "./cart.scss";
import ProductList from "../../components/ProductList/ProductList";




export function Cart(props) {

 
const { cardProducts, cart, cartModalActive, deleteFromCart } = useOutletContext();

  
 
    return (
      
        
          
        <div className="item-box row row-cols-2 row-cols-md-3 row-cols-lg-5 g-5">{cart.length?<ProductList
            cardProducts={cardProducts.filter(item => cart.includes(item.id))}
            
            handleModalActive={cartModalActive}
          
            addToFavorite={deleteFromCart}
          
           
            childrenProps={{
              buttonText: 'X',
              style:{display:"none"}}} />: <i className="empty-text">Ваша корзина порожня, переходьте до головної сторінки і обирайте!</i>} 
        
            
            </div>  
            
            
          // </div>
  

    )
}
