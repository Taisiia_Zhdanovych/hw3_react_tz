import { useOutletContext } from "react-router-dom";
import ProductList from "../components/ProductList/ProductList";

function StartPage(props) {
    const { cardProducts, favorits, addToFavorite, handleModalActive } = useOutletContext();
    return  <ProductList
               cardProducts={cardProducts}
               favorits={favorits}
               addToFavorite={addToFavorite}
               handleModalActive={handleModalActive}/>
}

export default StartPage;